# make sure this has been created on your local db. Although, bootstrap.sh should have taken care of it.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'betty',
        'USER': 'betty_user',
        'PASSWORD': '<your local db pwd>',
        'HOST': '127.0.0.1',   # Or an IP Address that your DB is hosted on
        'PORT': '3306',
        'TEST': {
            'NAME': 'test_betty',
        },
    }
}

SECRET_KEY = ''
ALLOWED_HOSTS = []
DEBUG = True

EMAIL_HOST_USER = ''
EMAIL_HOST= 'smtp.sendgrid.net'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_PASSWORD = ''
