#!/usr/bin/env bash
mysql -h localhost -P 3306 -uroot -e "CREATE USER 'betty_user'@'localhost' IDENTIFIED BY 'betty_app_password';"
mysql -h localhost -P 3306 -uroot -e "GRANT ALL PRIVILEGES ON * . * TO 'betty_user'@'localhost';"
mysql -h localhost -P 3306 -uroot -e "drop database if exists betty; create database betty DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;"
python manage.py migrate
python manage.py loaddata db.json