#!/usr/bin/env bash
python manage.py dumpdata --exclude contenttypes --exclude auth.permission --exclude admin.logentry --indent=4 --> db.json