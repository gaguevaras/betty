#!/usr/bin/env bash
current_dir=$PWD

if [[ $# -eq 0 ]] ; then
    echo 'You must provide an environment to push to: [staging|production]'
    exit 0
fi

cd /Users/gustavoguevara/betty
git subtree push --prefix betty $1 master
cd $current_dir
