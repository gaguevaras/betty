#!/usr/bin/env bash
if [ $# -eq 0 ]
  then
    mysql -h localhost -P 3306 -ubetty_user -pbetty_app_password -e "drop database if exists betty; create database betty DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;"
    python manage.py migrate
    python manage.py loaddata db.json
fi
