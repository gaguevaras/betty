import urllib
import urllib2

from bs4 import BeautifulSoup

url = 'http://localhost:63342/betty/reports/templates/course_welcome_report.html'
soup = BeautifulSoup(urllib2.urlopen(url).read(), "html5lib")
image_tags = soup.findAll("img")
for tag in image_tags:
    print tag['src']
    urllib.urlretrieve(tag['src'], "/Users/gustavoguevara/betty/E3/welcome_course_report/%s" % tag['src'].split('/')[-1])
