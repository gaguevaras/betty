$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd"
    });
    $('.multiselect_field').multiselect({
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        buttonClass: 'form-control',
    });

});

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function start_course(url) {
    var csrftoken = getCookie('bettyeducation_csrftoken');
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.post(url, function (response) {
        $('#alert_error').hide();
        $('#alert_success #message').text(response.message);
        $('#alert_success').show();
        setTimeout(function () {
            location.reload()
        }, 1000);

    }).fail(function (response) {
        $('#alert_success').hide();
        $('#alert_error #message').text(response.responseJSON.message);
        $('#alert_error').show();
    });
}

function end_course(url) {
    var csrftoken = getCookie('bettyeducation_csrftoken');
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.post(url, function (response) {
        $('#alert_error').hide();
        $('#alert_success #message').text(response.message);
        $('#alert_success').show();
        setTimeout(function () {
            location.reload()
        }, 1000);
    }).fail(function (response) {
        try {
            $('#alert_success').hide();
            $('#alert_error #message').text(response.responseJSON.message);
            $('#alert_error').show();
        } catch (err) {
            $('#alert_error #message').text("We were not able to end the course. Check that the course is over " +
                "and that all activities have scores. Otherwise contact support@bettyeducation.co");
            $('#alert_error').show();
            console.log(response);
            console.log(err);
            throw err;
        }
    });
}

function end_period(url, period_id) {
    var csrftoken = getCookie('bettyeducation_csrftoken');
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.post(url, function (response) {
        $('#alert_error_' + period_id).hide();
        $('#alert_success_' + period_id + ' #message').text(response.message);
        $('#alert_success_' + period_id).show();
        setTimeout(function () {
            location.reload()
        }, 1000);
    }).fail(function (response) {
        try {
            $('#alert_success_' + period_id).hide();
            $('#alert_error_' + period_id + ' #message').text(response.responseJSON.message);
            $('#alert_error_' + period_id).show();
        } catch (err) {
            $('#alert_error_' + period_id + ' #message').text("We were not able to end the period. Check " +
                "Please contact support@bettyeducation.co");
            $('#alert_error_' + period_id).show();
            console.log(response);
            console.log(err);
            throw err;
        }

    });
}

function update_activity(obj, url, period_id) {
    period_id = period_id || '';
    var csrftoken = getCookie('bettyeducation_csrftoken');
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    var data = {};
    var key = 'score';
    if (obj.name !== 'score') {
        key = 'comment';
    }
    data[key] = obj.value;

    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: 'json',
        success: function (response) {
            var success_alert = '#alert_success';
            if (period_id) {
                success_alert = '#alert_success_' + period_id;
            }
            $('#alert_error').hide();
            $(success_alert + ' #message').text(response.message);
            $(success_alert).show();
            if (obj.id) {
                $('#' + obj.id).prop('title', obj.value);
            }
            $('#alert_' + obj.getAttribute('data-activity')).show();
            setTimeout(function () {
                $(success_alert).hide();
            }, 2000);
        }
    }).fail(function (response) {
        var message = 'Error during update';
        var old_score = '';
        if (response.responseJSON) {
            message = response.responseJSON.errors.score[0];
            old_score = response.responseJSON.old_value;
        }
        $('#alert_success').hide();
        var error_alert = '#alert_error'
        if (period_id) {
            error_alert = '#alert_error_' + period_id;
        }
        $(error_alert + ' #message').text(message);
        $(error_alert).show();
        $('#alert_' + obj.getAttribute('data-activity')).hide();
        obj.value = old_score ? old_score : '';
        setTimeout(function () {
            $(error_alert).hide();
        }, 2000);
    });
}

function showComment(activity_id, button) {
    var activity = document.getElementById(activity_id);
    activity.classList.remove("hidden");
    button.classList.add("hidden");
}

function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("student_table");
    switching = true;
    //Set the sorting direction to ascending:
    dir = "asc";
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            /*check if the two rows should switch place,
            based on the direction, asc or desc:*/
            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //Each time a switch is done, increase this count by 1:
            switchcount++;
        } else {
            /*If no switching has been done AND the direction is "asc",
            set the direction to "desc" and run the while loop again.*/
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
    var columns = table.getElementsByTagName("TR")[0].getElementsByTagName("TH");
    for (var i = 0; i < columns.length - 1; i++) {
        document.getElementById('col-' + i).classList.remove('caret');
    }
    document.getElementById('col-' + n).classList.add('caret');
}
