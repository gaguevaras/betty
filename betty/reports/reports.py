import datetime

import pytz
from betty import settings
from courses.models import Student, Course, ClientCompany, CoursePeriod
from django.core.mail import send_mail
from django.db.transaction import atomic
from django.template import loader
from models import Report
from users.models import Company


def trigger_student_report(course_obj, student_obj, period_obj=None):
    student = Student.objects.get(pk=student_obj.id)
    template = loader.get_template('student_course_report.html')

    student_report = StudentReportManager(
        template=template,
        recipients=[student.email],
        course=course_obj,
        student=student_obj,
        company_id=student.company_id,
        period=period_obj
    )
    student_report.send_report()


@atomic
def trigger_course_start_report(course_id):
    """
    This triggers sending a report to all students in the course, to the teacher and the admin users for the company.
    :param course_id:
    :return:
    """
    template = loader.get_template('course_welcome_report.html')
    course = Course.objects.get(pk=course_id)
    students = Student.objects.filter(course=course)

    recipients = list(students.values_list('email', flat=True))
    recipients.extend(list(Company.objects.admin_users(course.company_id).values_list('email', flat=True)))
    recipients.extend([course.teacher.email])

    student_report = CourseWelcomeReportManager(
        template=template,
        recipients=recipients,
        course=course,
        company_id=course.company_id
    )
    student_report.send_report()


def trigger_manager_report(course, period=None):
    template = loader.get_template('manager_course_report.html')
    recipients = list(Company.objects.admin_users(course.company_id).values_list('email', flat=True))

    manager_report = AdminReportManager(
        template=template,
        recipients=recipients,
        course=course,
        company_id=course.company_id,
        period=period,
        title='Report for course %s' % course.code if period is None else 'Report for period %s in %s' % (period, course.code)

    )
    manager_report.send_report()


def trigger_client_report(course, period=None):
    client_company = ClientCompany.objects.get(pk=course.client_id)

    if not client_company.notifications_active:
        return

    template = loader.get_template('manager_course_report.html')
    recipients = [client_company.contact_email]

    manager_report = AdminReportManager(
        template=template,
        recipients=recipients,
        course=course,
        company_id=course.company_id,
        period=period,
        title='Report for course %s' % course.code if period is None else 'Report for period %s in %s' % (period, course.code)
    )
    manager_report.send_report()


def trigger_course_incomplete_report(course_id, period_id=None):
    template = loader.get_template('course_incomplete_report.html')
    course = Course.objects.get(pk=course_id)

    period = None
    if period_id is not None:
        period = CoursePeriod.objects.get(pk=period_id)

    recipients = list(Company.objects.admin_users(course.company_id).values_list('email', flat=True))
    recipients.append(course.teacher.email)

    teacher_report = TeacherReportManager(
        template=template,
        recipients=recipients,
        course=course,
        company_id=course.company_id,
        period=period
    )
    teacher_report.send_report()


class ReportManager:

    context = None
    recipients = None
    template = None
    results = None
    company = None
    course = None
    name = None

    def __init__(self, template, recipients, **kwargs):
        self.recipients = recipients
        self.template = template
        self.company = Company.objects.get(pk=kwargs.get('company_id'))
        self.course = kwargs.get('course')
        self._validate()

        self.context = self.get_report_context(**kwargs)
        self.context['STATIC_FILE_BUCKET_NAME'] = settings.BUCKET_NAME

    def _validate_context(self):
        if 'title' not in self.context:
            raise Exception('Invalid title for report.')

    def _validate_recipients(self):
        if not isinstance(self.recipients, list):
            raise Exception('Invalid recipients value. It must be a list.')

    def send_report(self):
        #save a record of this report
        from_email = "%s %s <%s>" % ("Betty from ", self.company.name, settings.DEFAULT_FROM_EMAIL)

        send_mail(
            subject=self.context['title'],
            message=self.template.render(self.context),
            html_message=self.template.render(self.context),
            from_email=from_email,
            recipient_list=self.recipients,
            fail_silently=False,
        )

        Report.objects.create(
            subject=self.context['title'],
            context=self.context,
            html_message=self.template.render(self.context),
            from_email=from_email,
            recipient_list=', '.join(self.recipients),
            date_created=datetime.datetime.now(pytz.utc),
            course=self.course,
            type=self.name
        )


    def _validate(self):
        self._validate_template()
        self._validate_recipients()
        if self.course is None:
            raise Exception('Invalid course value. It must be defined.')
        if self.name is None:
            raise Exception('Invalid report name. Give your report class a name attribute!.')

    def _validate_template(self):
        pass

    def get_report_context(self, **kwargs):
        """
        Should return a dict with the attributes used by the template passed in to the report manager subclass.
        :param kwargs: Contains the necessary parameters to build the report context dict.
        :return: The report context dict used by the template to render the report.
        """
        raise Exception('Must be overridden by a subclass.')


class StudentReportManager(ReportManager):

    name = 'student_notification'

    def get_report_context(self, **kwargs):
        student = kwargs.get('student')
        course = kwargs.get('course')
        period = kwargs.get('period')

        activities = Student.objects.get_activities(kwargs.get('company_id'), student.id, course.id, period.id if period is not None else None, include_comments=False)

        student_activity_list = list(activities)
        activity_titles = [x.name for x in student_activity_list]

        return {
            'student': student,
            'activity_list': student_activity_list,
            'titles': activity_titles,
            'title': 'Student Report for Course %s' % (course.code),
            'course': course,
            'period': period
        }


class AdminReportManager(ReportManager):

    name = 'admin_notification'

    def get_report_context(self, **kwargs):
        course = kwargs.get('course')
        period = kwargs.get('period')

        activities = {}
        activity_list = []

        students = Student.objects.filter(course=course)
        for student in students:
            activity_list = list(
                Student.objects.get_activities(
                    company=student.company_id,
                    student=student.id,
                    course=course.id,
                    period=period.id if period is not None else None
                )
            )
            activities[int(student.id)] = activity_list

        # Get the activity titles from the activities for the last student
        return {
            'title': kwargs.get('title'),
            'titles': [x.name for x in activity_list],
            'activities': activities,
            'course': course,
            'period': period
        }


class TeacherReportManager(ReportManager):

    name = 'teacher_notification'

    def get_report_context(self, **kwargs):
        course = kwargs.get('course')
        period = kwargs.get('period')

        # Get the activity titles from the activities for the last student
        return {
            'title': 'Scores are incomplete for course %s' % course.code,
            'course': course,
            'period': period
        }


class CourseWelcomeReportManager(ReportManager):

    name = 'course_welcome_notification'

    def get_report_context(self, **kwargs):
        course = kwargs.get('course')

        # Get the activity titles from the activities for the last student
        return {
            'title': 'Welcome to your course at %s!' % course.company.name,
            'course': course,
            'terms_and_conditions_href': 'https://storage.googleapis.com/%s/templates/tyc.pdf' % settings.BUCKET_NAME,
            # 'portfolio_href': 'https://storage.googleapis.com/%s/templates/portfolios/Portfolio%s.pdf' % (settings.BUCKET_NAME, course.level.name.replace(" ", ""))
            'portfolio_href': 'https://e3english.com/mi-portafolio/'
        }