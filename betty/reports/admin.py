import re
from json import loads

from celery import shared_task
from django.conf import settings
from django.contrib import admin

# Register your models here.
from django.core.mail import send_mail

from models import Report


@shared_task
def send_report_task(report_object):
    from_email = "%s %s <%s>" % ("Betty from ", report_object.course.company.name, settings.DEFAULT_FROM_EMAIL)
    recipient_list = re.findall(r'[\w\._\+%-]+@[\w\.-]+\.\w+', report_object.recipient_list)
    recipient_list.append(settings.DEFAULT_FROM_EMAIL)
    subject = "[resent] %s" % report_object.subject
    send_mail(
        subject=subject,
        message=report_object.html_message,
        html_message=report_object.html_message,
        from_email=from_email,
        recipient_list=recipient_list,
        fail_silently=False,
    )


def resend_report(modeladmin, request, queryset):
    for obj in queryset:
        send_report_task(obj)


resend_report.short_description = "Resend reports"

class ReportAdmin(admin.ModelAdmin):
    list_filter = ('course',)
    list_display = ('course', 'type', 'date_created', 'recipient_list')
    actions = [resend_report]

admin.site.register(Report, ReportAdmin)
