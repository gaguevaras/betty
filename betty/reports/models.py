from __future__ import unicode_literals

from django.db import models


class Report(models.Model):
    """
    The different activities available for students enroled in a course.
    They can be 'test', 'skill', 'total', 'pass/fail'
    """

    subject = models.TextField(null=True, blank=True)
    context = models.TextField(null=True, blank=True)
    html_message = models.TextField(null=True, blank=True)
    from_email = models.TextField(null=True, blank=True)
    recipient_list = models.TextField(null=True, blank=True)
    date_created = models.DateTimeField()
    course = models.ForeignKey('courses.Course', null=True, blank=True)
    type = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{0} {1}".format(str(self.date_created), self.course)

    def __unicode__(self):
        return "{0} {1}".format(str(self.date_created), self.course)
