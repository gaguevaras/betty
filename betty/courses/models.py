from __future__ import unicode_literals

import logging
from datetime import datetime, date

from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from users.models import Company, UserManager, UserRole

# Get an instance of a logger
logger = logging.getLogger(__name__)

class Level(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name.decode("utf-8")


class ClientCompany(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200, null=True)
    contact_name = models.CharField(max_length=100, null=True)
    contact_email = models.EmailField(null=True)
    company = models.ForeignKey(Company)
    notifications_active = models.BooleanField(default=True)
    sales_associates = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return self.name.decode("utf-8")

    def __unicode__(self):
        return self.name


class StudentManager(models.Manager):

    def create_student(self, cleaned_data, company_id):
        new_student = Student.objects.create(
            name=cleaned_data['name'],
            email=cleaned_data['email'],
            phone=cleaned_data['phone'],
            client=cleaned_data['client'],
            company=Company.objects.get(pk=company_id)
        )
        new_student.sales_associates = cleaned_data['sales_associates']
        new_student.save()
        return new_student

    def get_activities(self, company, student, course, period=None, include_comments=True):
        queryset = Activity.objects.filter(company=company, student=student, course=course).order_by('type', 'name')
        if period is not None:
            queryset = queryset.filter(period=period)
        if not include_comments:
            queryset = queryset.filter(~Q(type='feedback') & ~Q(type='period_feedback'))
        return queryset


class Student(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(null=True)
    phone = models.CharField(max_length=50, null=True, blank=True)
    client = models.ForeignKey(ClientCompany, null=True, blank=True)
    company = models.ForeignKey(Company)
    sales_associates = models.ManyToManyField(User, blank=True)

    objects = StudentManager()

    def __str__(self):
        return "%s - %s" % (self.client.name, self.name)

    def __unicode__(self):
        return "%s - %s" % (self.client.name, self.name)


class TeacherManager(models.Manager):

    def create_teacher(self, cleaned_data, company_id):
        
        new_user = User.objects.create(
            first_name = cleaned_data['first_name'],
            last_name = cleaned_data['last_name'],
            username=cleaned_data['username'],
            email=cleaned_data['email'],
        )
        new_user.set_password('changeme')

        new_user.save()

        user_manager = UserManager()
        new_profile = user_manager.create_profile(new_user, Company.objects.get(pk=company_id), [UserRole.objects.get(name='teacher')])

        new_teacher = Teacher.objects.create(
            name=cleaned_data['first_name'] + ' ' + cleaned_data['last_name'],
            email=cleaned_data['email'],
            phone=cleaned_data['phone'],
            country=cleaned_data['country'],
            specialty=cleaned_data['specialty'],
            company=Company.objects.get(pk=company_id),
            user=new_user
        )

        return new_teacher


class Teacher(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(null=True)
    phone = models.CharField(max_length=50, null=True)
    company = models.ForeignKey(Company)
    user = models.OneToOneField(User)
    country = models.CharField(max_length=50, null=True)
    specialty = models.TextField(null=True)

    objects = TeacherManager()

    def __str__(self):
        return self.name.decode("utf-8")

    def __unicode__(self):
        return self.name


class Course(models.Model):

    # Code (CLIENT_COMPANY-SEQUENCE)
    code = models.CharField(max_length=100)
    # Level
    level = models.ForeignKey(Level, null=True, blank=True)
    # Company
    company = models.ForeignKey(Company)
    # Client Company
    client = models.ForeignKey(ClientCompany, null=True, blank=True)
    # Address
    address = models.CharField(max_length=200)
    # Schedule
    # @todo:gustavo the way to store a course schedule should probably follow rrule standards
    schedule = models.CharField(max_length=200)
    # Start Date
    start_date = models.DateField()
    # End Date
    end_date = models.DateField()
    # List of Students
    students = models.ManyToManyField(Student, blank=True)

    teacher = models.ForeignKey(Teacher, null=True, on_delete=models.SET_NULL)

    is_active = models.BooleanField(default=True)

    duration = models.IntegerField(default=48)

    def is_over(self):
        now = datetime.now()
        if self.end_date + relativedelta(days=3) <= date(now.year, now.month, now.day):
            return True
        return False

    def __str__(self):
        return self.code.decode("utf-8")

    def __unicode__(self):
        return "%s - %s" % (self.code, self.level)


class CoursePeriod(models.Model):

    course = models.ForeignKey(Course)
    start_date = models.DateField()
    end_date = models.DateField()
    is_active = models.BooleanField()

    def is_in_buffer(self):
        """
        This returns whether the period is in it's final days or buffer.
        :return: (boolean) is in buffer.
        """
        now = datetime.now()
        if self.get_buffer_start_date() <= date(now.year, now.month, now.day):
            return True
        return False

    def get_buffer_start_date(self):
        return self.end_date + relativedelta(days=-5)

    def __str__(self):
        return "%s to %s" % (self.start_date.strftime('%b %d, %Y'), self.end_date.strftime('%b %d, %Y'))


class ActivityManager(models.Manager):

    def create_activities(self, company, course, student):
        """
        This function creates activities for a given student in a course. It will find the company's activities set for
        their courses from CompanyActivity if they have been set. Otherwise, it will create a default set of activities.

        :param company:
        :param course:
        :param student:
        :return:
        """
        try:
            activities = list(CompanyActivity.objects.filter(company=company).values('name', 'type'))
            if len(activities) == 0:
                raise Exception('No activities found for company %s.' % company)
        except:
            activities = [
                {
                    'name': 'Comments',
                    'type': 'feedback'
                },
                {
                    'name': 'Test 1',
                    'type': 'test'
                },
                {
                    'name': 'Test 2',
                    'type': 'test'
                },
                {
                    'name': 'Test 3',
                    'type': 'test'
                },
            ]

        for activity in activities:
            # Create activities for each of the periods in the course
            if activity['type'] in ['skill', 'period_feedback']:
                for period in course.courseperiod_set.filter(is_active=True).order_by('start_date'):
                    Activity.objects.create(
                        company=company,
                        course=course,
                        student=student,
                        name=activity['name'],
                        type=activity['type'],
                        period=period
                    )
                continue
            # Create activities for the whole course (Final test and Portfolio)
            Activity.objects.create(
                company=company,
                course=course,
                student=student,
                name=activity['name'],
                type=activity['type'],
            )


class Activity(models.Model):
    """
    The different activities available for students enroled in a course.
    They can be 'test', 'skill', 'total', 'pass/fail'
    """

    name = models.CharField(max_length=100, blank=True)
    score = models.FloatField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    type = models.CharField(max_length=100, default='task')

    # Each activity is associated to one course
    course = models.ForeignKey(Course)
    # Each activity is associated to one student
    student = models.ForeignKey(Student)
    company = models.ForeignKey(Company)

    period = models.ForeignKey(CoursePeriod, null=True, blank=True)

    objects = ActivityManager()

    def __str__(self):
        return self.name.decode("utf-8")

    def __unicode__(self):
        return self.name


class CompanyActivity(models.Model):
    """
    Default set of activities for courses created in a given company
    """
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    company = models.ForeignKey(Company)

    def __str__(self):
        return self.name.decode("utf-8")

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "company activities"


class CompanySettings(models.Model):
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=100)
    company = models.ForeignKey(Company)

    def __str__(self):
        return self.name.decode("utf-8")

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "company settings"

