from django.core.exceptions import ObjectDoesNotExist

from betty import settings
from betty.decorators import role_required
from courses.forms import CourseCreationForm, ClientCompanyCreationForm, StudentCreationForm, TeacherCreationForm, \
    ActivityCreationForm, ActivityUpdateForm, CourseUpdateForm, CoursePeriodCreationForm, StudentListFiltrationForm
from courses.mixins import AjaxFormResponseMixin
from courses.models import Course, ClientCompany, Student, Teacher, Activity, CompanySettings, CoursePeriod
from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.db.models import Q, Avg, Sum
from django.db.transaction import atomic
from django.forms import formset_factory
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import FormView, ListView, DetailView, UpdateView, DeleteView, TemplateView
from reports.models import Report
from reports.reports import trigger_manager_report, trigger_client_report, trigger_student_report, \
    trigger_course_incomplete_report, CourseWelcomeReportManager
from users.models import Company

from courses.tasks import send_end_of_period_reports, send_course_welcome_reports
from dateutil import rrule

import logging
logger = logging.getLogger(__name__)

@method_decorator(login_required, name='dispatch')
class CourseCreationView(FormView):
    template_name = 'course_create.html'
    form_class = CourseCreationForm
    created_course_id = None

    def get_form_kwargs(self):
        kwargs = super(CourseCreationView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        company = Company.objects.get(pk=self.request.user.userprofile.company_id)
        course_code = self.get_course_code(company, form.cleaned_data['client'])

        new_course = Course.objects.create(
            code=course_code,
            level=form.cleaned_data['level'],
            company=company,
            client=form.cleaned_data['client'],
            address=form.cleaned_data['address'],
            schedule=form.cleaned_data['schedule'],
            start_date=form.cleaned_data['start_date'],
            end_date=form.cleaned_data['end_date'],
            teacher=form.cleaned_data['teacher']
        )
        students = form.cleaned_data['students'].all()
        new_course.students.set(students)
        self.created_course_id = new_course.id

        dtstart = form.cleaned_data['start_date']
        dtend = form.cleaned_data['end_date']

        firsts = [event.date() for event in rrule.rrule(freq=rrule.MONTHLY, bymonthday=1, dtstart=dtstart, until=dtend)]

        # @todo:gustavo if we are going to be flexible here's where the flexibility should be entered
        # when entering the dtstart in the sequence, remove the first first if it's too close, or something of the sort
        if dtstart < firsts[0]:
            firsts.insert(0, dtstart)

        # Same for the last period
        if dtend > firsts[len(firsts)-1]:
            firsts.insert(len(firsts), dtend)

        # Create the course's periods
        if len(firsts) > 1:
            # iterate over dates except the last one
            for i in range(0, len(firsts)-1):
                CoursePeriod.objects.create(
                    course=new_course,
                    start_date=firsts[i],
                    end_date=firsts[i+1] + relativedelta(days=-1),
                    is_active=True
                )

        # create activities for each of the enrolled students
        for student in students:
            Activity.objects.create_activities(company, new_course, student)

        return super(CourseCreationView, self).form_valid(form)

    def get_success_url(self):
        return reverse('course_detail', kwargs={'pk': self.created_course_id})

    def get_course_code(self, company, client=None):

        if client is not None:
            client_code = client.name.upper().replace(" ", "_")
            course_number = Course.objects.filter(company=company, client=client).count()
        else:
            client_code = 'IND'
            course_number = str(Course.objects.filter(company=company, client=None).count())

        code = '%s-%s' % (client_code, (course_number + 1))

        return code


@method_decorator(login_required, name='dispatch')
class CourseUpdateView(UpdateView):
    model = Course
    template_name = 'course_update.html'
    success_url = '/courses/list/'
    form_class = CourseUpdateForm
    context_object_name = 'course'

    def get_form_kwargs(self):
        kwargs = super(CourseUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        # check new students
        for student in form.cleaned_data['students']:
            # Only create new activities if the included user doesn't have any for this course in this company
            if Activity.objects.filter(company=self.object.company, course=self.object, student=student).count() == 0:
                # @todo:gustavo only create activities of the types already existing for this course
                Activity.objects.create_activities(self.object.company, self.object, student)
        # Remove activities for removed students
        for removed_student in self.object.students.all().exclude(id__in=form.cleaned_data['students'].values('id')):
            Activity.objects.filter(student=removed_student, course=self.object).delete()

        return super(CourseUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('course_detail', kwargs={'pk': self.kwargs['pk']})


@method_decorator(login_required, name='dispatch')
class CourseDetailView(DetailView):
    model = Course
    template_name = 'course_detail.html'
    context_object_name = 'course'

    def get_context_data(self, **kwargs):
        context = super(CourseDetailView, self).get_context_data(**kwargs)
        # Create a list of activities for each of the students enroled in the course
        # Ensure that the list of activities contains the activities in the same order for all students.
        activities = {}
        activity_list = []
        course = Course.objects.get(pk=self.kwargs['pk'])
        students = Student.objects.filter(course=course)
        for student in students:
            activity_list = list(
                Student.objects.get_activities(student.company_id, student.id, self.kwargs['pk']))
            activities[int(student.id)] = activity_list

        # Get the activity titles from the activities for the last student
        first_student_activity_list = list(Student.objects.get_activities(students.first().company_id, students.first().id, self.kwargs['pk']))
        context['titles'] = [x.name for x in first_student_activity_list if x.type not in ['skill', 'period_feedback']]
        # Get the skill titles from the activities for the last student
        context['skill_titles'] = [x.name for x in first_student_activity_list if
                                   x.type in ['skill', 'period_feedback'] and x.period == self.get_object().courseperiod_set.first()]

        # Helper context attribute to easily iterate over the courses' periods on the view.
        context['periods'] = self.get_object().courseperiod_set.order_by('start_date', 'end_date').all()

        # The actual activities to be shown.
        context['activities'] = activities

        context['test_range'] = "0 - %s" % "{:.0f}".format(float(CompanySettings.objects.get(
            company=course.company_id,
            name="test_max_score"
        ).value))

        context['attendance_range'] = "0 - 100"

        context['skill_range'] = "0 - %s" % "{:.0f}".format(float(CompanySettings.objects.get(
            company=course.company_id,
            name="skill_max_score"
        ).value))

        roles = self.request.user.userprofile.roles
        if roles.filter(Q(name__contains='admin') | Q(name__contains='manager') | Q(name__contains='teacher')).exists():
            context['course_students'] = self.get_object().students.all()
        elif roles.filter(Q(name__contains='sales')).exists():
            context['course_students'] = self.get_object().students.filter(sales_associates__id=self.request.user.id)

        return context


@method_decorator(login_required, name='dispatch')
class CoursesListView(ListView):
    template_name = 'course_list.html'
    model = Course

    def get_queryset(self):
        # if not manager or admin its a teacher or sales
        if not self.request.user.userprofile.roles.filter(Q(name__contains='admin') | Q(name__contains='manager')).exists():
            # if a teacher
            try:
                queryset = Course.objects.filter(
                    company_id=self.request.user.userprofile.company_id,
                    teacher=self.request.user.teacher
                ).order_by('-is_active')
            except:
                queryset = Course.objects.filter(
                    Q(
                        company_id=self.request.user.userprofile.company_id,
                        client_id__in=ClientCompany.objects.filter(sales_associates__id=self.request.user.id).values_list('id')
                    ) |
                    Q(
                        company_id=self.request.user.userprofile.company_id,
                        students__sales_associates__id=self.request.user.id
                    )
                ).order_by('-is_active').distinct()

            return queryset

        return Course.objects.filter(company_id=self.request.user.userprofile.company_id).order_by('-is_active', 'end_date')


@method_decorator(login_required, name='dispatch')
class CourseDeleteView(DeleteView):
    template_name = 'confirm_delete.html'
    model = Course

    def get_success_url(self):
        return '/courses/list/'


@method_decorator(login_required, name='dispatch')
class StudentCreationView(FormView):
    template_name = 'student_create.html'
    success_url = '/courses/student/list/'
    form_class = StudentCreationForm

    def get_form_kwargs(self):
        kwargs = super(StudentCreationView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.create_student(self.request.user.userprofile.company_id)
        return super(StudentCreationView, self).form_valid(form)


@login_required
def manage_students(request, amount):
    StudentFormSet = formset_factory(StudentCreationForm, extra=int(amount))
    if request.method == 'POST':
        formset = StudentFormSet(request.POST, request.FILES, form_kwargs={'user': request.user})
        if formset.is_valid():
            for form in formset:
                if bool(form.cleaned_data):
                    form.create_student(request.user.userprofile.company_id)
        else:
            return render(request, 'manage_students.html', {'formset': formset})
        return HttpResponseRedirect('/courses/student/list/')
    else:
        formset = StudentFormSet(form_kwargs={'user': request.user})
    return render(request, 'manage_students.html', {'formset': formset})


@method_decorator(login_required, name='dispatch')
class StudentUpdateView(UpdateView):
    model = Student
    template_name = 'student_update.html'
    form_class = StudentCreationForm
    success_url = '/students/list/'
    context_object_name = 'student'

    def get_form_kwargs(self):
        kwargs = super(StudentUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self):
        return reverse('student_detail', kwargs={'pk': self.kwargs['pk']})


@method_decorator(login_required, name='dispatch')
class StudentDetailView(DetailView):
    model = Student
    template_name = 'student_detail.html'
    context_object_name = 'student'


@method_decorator(login_required, name='dispatch')
class StudentsListView(ListView):
    template_name = 'student_list.html'
    fields = ['student_name', 'student_email', 'student_phone', 'client']
    form_class = StudentListFiltrationForm
    model = Student

    def get_form_kwargs(self):
        kwargs = super(StudentsListView, self).get_form_kwargs()
        kwargs.update({'student_name': self.kwargs.get('student_name')})
        kwargs.update({'student_email': self.kwargs.get('student_email')})
        kwargs.update({'student_phone': self.kwargs.get('student_phone')})
        kwargs.update({'client': self.kwargs.get('client')})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(StudentsListView, self).get_context_data(**kwargs)
        if self.request.GET.get('student_name') is not None:
            context['student_name'] = self.request.GET.get('student_name')
        if self.request.GET.get('student_email') is not None:
            context['student_email'] = self.request.GET.get('student_email')
        if self.request.GET.get('student_phone') is not None:
            context['student_phone'] = self.request.GET.get('student_phone')
        if self.request.GET.get('client') is not None:
            context['client'] = self.request.GET.get('client')
        return context

    def get_queryset(self):
        student_name = self.request.GET.get('student_name', None)
        student_email = self.request.GET.get('student_email', None)
        student_phone = self.request.GET.get('student_phone', None)
        client = self.request.GET.get('client', None)

        # We are going to use Q which allows us to create more complex queries.
        # See: https://docs.djangoproject.com/en/1.11/topics/db/queries/
        criteria = Q(company_id=self.request.user.userprofile.company_id)

        # Look for students whose name attribute contains the filter passed in
        # See: https://docs.djangoproject.com/en/dev/ref/models/querysets/#std:fieldlookup-icontains
        if student_name is not None:
            criteria = criteria & Q(name__icontains=student_name)
        if student_email is not None:
            criteria = criteria & Q(email__icontains=student_email)
        if student_phone is not None:
            criteria = criteria & Q(phone__icontains=student_phone)
        # Look for clients whose name contains the filter passed in
        if client is not None:
            client_pks = ClientCompany.objects.filter(name__icontains=client).values_list('id')
            criteria = criteria & Q(client_id__in=client_pks)

        return Student.objects.filter(criteria)


@method_decorator(login_required, name='dispatch')
class StudentDeleteView(DeleteView):
    template_name = 'confirm_delete.html'
    model = Student

    def get_success_url(self):
        return '/courses/student/list/'


@method_decorator(login_required, name='dispatch')
class TeacherCreationView(FormView):
    template_name = 'teacher_create.html'
    form_class = TeacherCreationForm
    success_url = '/courses/teacher/list/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.create_teacher(self.request.user.userprofile.company_id)
        return super(TeacherCreationView, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class TeacherUpdateView(UpdateView):
    model = Teacher
    template_name = 'teacher_update.html'
    fields = ['name', 'email', 'phone', 'country', 'specialty']
    context_object_name = 'teacher'

    def get_success_url(self):
        return reverse('teacher_detail', kwargs={'pk': self.kwargs['pk']})


@method_decorator(login_required, name='dispatch')
class TeacherDetailView(DetailView):
    model = Teacher
    template_name = 'teacher_detail.html'
    context_object_name = 'teacher'


@method_decorator(login_required, name='dispatch')
class TeachersListView(ListView):
    template_name = 'teacher_list.html'
    model = Teacher

    def get_queryset(self):
        return Teacher.objects.filter(company_id=self.request.user.userprofile.company_id)


@method_decorator(login_required, name='dispatch')
class TeacherDeleteView(DeleteView):
    template_name = 'confirm_delete.html'
    model = Teacher

    def get_success_url(self):
        return '/courses/teacher/list/'


@method_decorator(login_required, name='dispatch')
class ClientCompanyCreationView(FormView):
    template_name = 'client_create.html'
    form_class = ClientCompanyCreationForm
    success_url = '/courses/client/list/'

    def get_form_kwargs(self):
        kwargs = super(ClientCompanyCreationView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.create_client(self.request.user)
        return super(ClientCompanyCreationView, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class ClientCompanyUpdateView(UpdateView):
    model = ClientCompany
    template_name = 'client_update.html'
    form_class = ClientCompanyCreationForm
    success_url = '/client/list/'
    context_object_name = 'client'

    def get_form_kwargs(self):
        kwargs = super(ClientCompanyUpdateView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_success_url(self):
        return reverse('client_detail', kwargs={'pk': self.kwargs['pk']})


@method_decorator(login_required, name='dispatch')
class ClientCompanyDetailView(DetailView):
    model = ClientCompany
    template_name = 'client_detail.html'
    context_object_name = 'client'


@method_decorator(login_required, name='dispatch')
class ClientCompanyListView(ListView):
    template_name = 'client_list.html'
    model = ClientCompany

    def get_queryset(self):
        return ClientCompany.objects.filter(company_id=self.request.user.userprofile.company_id)


@method_decorator(login_required, name='dispatch')
class ClientCompanyDeleteView(DeleteView):
    template_name = 'confirm_delete.html'
    model = ClientCompany

    def get_success_url(self):
        return '/courses/client/list/'


@method_decorator(login_required, name='dispatch')
class ActivityCreationView(FormView):
    template_name = 'activity_create.html'
    form_class = ActivityCreationForm

    def get_form_kwargs(self):
        kwargs = super(ActivityCreationView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        kwargs.update({'course': self.kwargs['course_id']})
        return kwargs

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.create_activity(self.request.user)
        return super(ActivityCreationView, self).form_valid(form)

    def get_success_url(self):
        return reverse('course_detail', kwargs={'pk': self.kwargs['course_id']})


@method_decorator(login_required, name='dispatch')
class ActivityUpdateView(UserPassesTestMixin, AjaxFormResponseMixin, UpdateView):
    model = Activity
    template_name = 'activity_update.html'
    form_class = ActivityUpdateForm
    authorized_roles = ['admin', 'manager', 'teacher']

    def test_func(self):
        if self.request.user.is_authenticated():
            if bool(self.request.user.userprofile.roles.filter(name__in=self.authorized_roles).count()) | self.request.user.is_superuser:
                return True
        return False

    def get_form_kwargs(self):
        kwargs = super(ActivityUpdateView, self).get_form_kwargs()
        kwargs.update({'course': self.kwargs['course_id']})
        return kwargs

    def get_success_url(self):
        return reverse('course_detail', kwargs={'pk': self.kwargs['course_id']})

    def get_serializable_data(self):
        return {
            'id': self.get_context_data()['activity'].id,
            'score': self.get_context_data()['activity'].score,
        }


@method_decorator(login_required, name='dispatch')
class ActivityDetailView(DetailView):
    model = Activity
    template_name = 'activity_detail.html'
    context_object_name = 'activity'


@method_decorator(login_required, name='dispatch')
class ActivityListView(ListView):
    template_name = 'activity_list.html'
    model = Activity

    def get_queryset(self):
        return Activity.objects.filter(company_id=self.request.user.userprofile.company_id)


@method_decorator(login_required, name='dispatch')
class ActivityDeleteView(DeleteView):
    template_name = 'confirm_delete.html'
    model = Activity

    def get_success_url(self):
        return '/courses/activity/list/'


@login_required
@role_required('admin', 'manager')
@atomic
def end_course(request, course_id):
    """
    This view method will end a course and calculate the total score for each student as well as their pass/fail score.

    :param request:
    :param course_id: The course id to end.
    :return:
    """

    # Check that the course exists for the given id
    try:
        course = Course.objects.get(pk=course_id)
    except ObjectDoesNotExist:
        response = {
            'message': 'The given course does not exist.' % course_id
        }
        return JsonResponse(response, status=400)

    # Check that the course belongs to the same company as the current user
    if course.company_id != request.user.userprofile.company_id:
        response = {
            'message': 'The given course does not exist.' % course_id
        }
        return JsonResponse(response, status=400)

    # Check that the course is over
    if not course.is_over():
        response = {
            'message': 'Course cannot be ended. The end date is has not yet been reached. Try again on %s.' % course.end_date
        }
        return JsonResponse(response, status=500)

    course_activities = Activity.objects.filter(course=course.id)
    scorable_activities = course_activities.filter(Q(type__exact='test') | Q(type__exact='skill'))
    incomplete_course_activities = scorable_activities.filter(score__isnull=True)
    if incomplete_course_activities.exists():
        trigger_course_incomplete_report(course.id)
        response = {
            'incomplete_activities': list(incomplete_course_activities.values_list('id')),
            'message': 'Course cannot be ended. Some activities have not been graded. A request to complete scores has been sent to %s' % course.teacher.name
        }
        return JsonResponse(response, status=500)

    try:
        pass_threshold_percentage = float(
            CompanySettings.objects.get(company=course.company_id, name='pass_threshold_percentage').value)
        max_score = float(CompanySettings.objects.get(company=course.company_id, name='test_max_score').value)
        threshold = (pass_threshold_percentage / 100) * max_score
    except:
        threshold = (float(settings.DEFAULT_PASS_THRESHOLD_PERCENTAGE) / 100) * settings.DEFAULT_MAX_SCORE

    # calculate average of test activities for each student
    for student in course.students.all():
        student_activities = course_activities.filter(student=student)
        # @todo:gustavo attendance prevent attendance from counting towards pass/fail status
        test_student_activities = student_activities.filter(type='test').exclude(name='Attendance')

        score_aggregates = test_student_activities.aggregate(sum=Sum('score'), average=Avg('score'))
        total = student_activities.get(type='total')
        total.score = score_aggregates['sum']
        total.save()

        # calculate pass/fail for each student
        pass_fail = student_activities.get(type='pass/fail')
        pass_fail.score = 1 if score_aggregates['average'] >= threshold else 0
        pass_fail.save()

    # Send manager report
    trigger_manager_report(course)
    # Send HR report
    trigger_client_report(course)
    # Send student reports
    for student in course.students.all():
        trigger_student_report(course, student)

    course.is_active = False
    course.save()

    response = {
        'course_id': course.id,
        'message': 'Course has ended successfully.'
    }
    return JsonResponse(response, status=200)


@login_required
@role_required('admin', 'manager')
def start_course(request, course_id):
    # Get all of the course's activities
    if Report.objects.filter(course=course_id, type=CourseWelcomeReportManager.name).exists():
        response = {
            'message': 'A welcome message for this course has already been sent!'
        }
        return JsonResponse(response, status=400)
    send_course_welcome_reports.delay(course_id)
    response = {
        'message': 'A message will be sent to all students.'
    }
    return JsonResponse(response, status=200)


@login_required
@role_required('admin', 'manager')
@atomic
def end_period(request, course_id, period_id):
    """
    This view endpoint ends a period in a given course.
    :param request:
    :param course_id:
    :param period_id:
    :return:
    """

    # @todo:gustavo Do this in a celery task and make sure emails are only sent after successfully saving the period.
    try:
        period = CoursePeriod.objects.get(pk=period_id)
        course = Course.objects.get(pk=course_id)
    except:
        return JsonResponse({
            'message': 'The specified period or course do not exist.'
        }, status=400)

    if period.activity_set.filter(score__isnull=True, type='skill').exists():
        send_end_of_period_reports.delay(course.id, period.id, incomplete=True)
        response = {
            'incomplete_activities': list(period.activity_set.filter(score__isnull=True).values_list('id')),
            'message': """Period cannot be ended.
                            Some activities have not been graded.
                            A request to complete scores has been sent to %s""" % period.course.teacher.name
        }
        return JsonResponse(response, status=400)

    if not period.is_in_buffer():
        response = {
            'message': 'Period cannot be ended. The end date is has not yet been reached. Try again on %s.' %
                       period.get_buffer_start_date().strftime('%b %d, %Y')
        }
        return JsonResponse(response, status=400)

    if not period.is_active:
        response = {
            'message': 'Period %s is already over. An admin has already ended this period.' % period_id
        }
        return JsonResponse(response, status=400)

    # @todo:gustavo Why does this save persist even if there's an exception later? Shouldn't @atomic roll this back?
    # Answer: Looks like catching exceptions inside atomic blocks prevents transaction rollbacks, so this might need
    # to be done inside a controller for CoursePeriod entities.
    period.is_active = False
    period.save()

    # Send reports to each student in the course about the ending period.
    send_end_of_period_reports.delay(course.id, period.id)

    response = {
        'course_id': course_id,
        'period_id': period_id,
        'message': 'Period has ended successfully.'
    }
    return JsonResponse(response, status=200)


@method_decorator(login_required, name='dispatch')
class CoursePeriodUpdateView(UpdateView):
    model = CoursePeriod
    template_name = 'period_update.html'
    context_object_name = 'period'
    form_class = CoursePeriodCreationForm

    def get_form_kwargs(self):
        kwargs = super(CoursePeriodUpdateView, self).get_form_kwargs()
        kwargs.update({'course_id': self.object.course.id})
        return kwargs

    def get_success_url(self):
        return reverse('course_detail', kwargs={'pk': self.object.course.id})


@method_decorator(login_required, name='dispatch')
class CoursePeriodDeleteView(DeleteView):
    template_name = 'confirm_delete.html'
    model = CoursePeriod

    def get_success_url(self):
        return reverse('course_detail', kwargs={'pk': self.object.course.id})


@method_decorator(login_required, name='dispatch')
class CoursePeriodCreationView(FormView):
    template_name = 'period_create.html'
    model = CoursePeriod
    fields = ['start_date', 'end_date']
    context_object_name = 'period'
    form_class = CoursePeriodCreationForm

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.create_period(self.kwargs['course_id'])
        return super(CoursePeriodCreationView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(CoursePeriodCreationView, self).get_form_kwargs()
        kwargs.update({'course_id': self.kwargs.get('course_id')})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CoursePeriodCreationView, self).get_context_data(**kwargs)
        context['course'] = Course.objects.get(pk=self.kwargs.get('course_id'))
        return context

    def get_success_url(self):
        return reverse('course_detail', kwargs={'pk': self.kwargs.get('course_id')})


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        try:
            logo = CompanySettings.objects.get(company=self.request.user.userprofile.company, name='company_logo_href').value
        except:
            logo = settings.BETTY_LOGO_HREF
        context['company_logo'] = logo

        return context


def raise_error(request):
    from courses.tasks import _raise_error
    _raise_error()
    return JsonResponse({}, status=200)


def raise_error_delay(request):
    from courses.tasks import _raise_error
    _raise_error.delay()
    return JsonResponse({}, status=200)