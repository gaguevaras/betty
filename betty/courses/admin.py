from courses.models import CompanyActivity, Course, CompanySettings, Level
from django.contrib import admin


# Register your models here.
class CompanyActivityAdmin(admin.ModelAdmin):
    fields = ('name', 'type', 'company')
    list_display = ('name', 'type', 'company')
    list_filter = ('company',)

admin.site.register(CompanyActivity, CompanyActivityAdmin)


# Register your models here.
class CompanySettingsAdmin(admin.ModelAdmin):
    fields = ('name', 'value', 'company')
    list_display = ('name', 'value', 'company')
    list_filter = ('company',)

admin.site.register(CompanySettings, CompanySettingsAdmin)


class CourseAdmin(admin.ModelAdmin):
    list_display = ('code', 'company', 'client', 'address', 'schedule', 'start_date', 'end_date', 'teacher', 'is_active')

admin.site.register(Course, CourseAdmin)


class LevelAdmin(admin.ModelAdmin):
    pass

admin.site.register(Level, LevelAdmin)
