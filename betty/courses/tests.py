from __future__ import unicode_literals
import datetime

from betty import settings
from django.test import TestCase, LiveServerTestCase
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from .models import Course, Teacher


# Create your tests here.

class CoursesMethodTest(TestCase):
    fixtures = ['db.json']

    def test_course_create_method(self):
        time = timezone.now() - datetime.timedelta(days=30)
        self.assertIsNot(Course.objects.create(start_date=time, end_date=time, company_id=1),
                         Course.objects.create(start_date=timezone.now(),
                                               end_date=timezone.now() + datetime.timedelta(days=30), company_id=1))

class MySeleniumTests(LiveServerTestCase):
    fixtures = ['db.json']

    @classmethod
    def setUpClass(cls):
        super(MySeleniumTests, cls).setUpClass()
        cls.selenium = WebDriver(executable_path=settings.CHROMEDRIVER_PATH)
        cls.selenium.implicitly_wait(60)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(MySeleniumTests, cls).tearDownClass()

    def login(self, username, password):
        # Login with betty
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys(username)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys(password)
        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()

    def test_login(self):
        # Go to the login page and enter the WRONG credentials
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('betty')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('not_the_real_password')
        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()
        self.assertIn(
            'Please enter a correct username and password.',
            self.selenium.find_element_by_class_name('errorlist').text
        )
        # Now enter the right password and try to login
        self.selenium.find_element_by_name("password").send_keys('changeme')
        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()
        # Now use the css selector for the Clients menu on the navbar to see if it is displayed.
        # This means the user logged in successfully!
        clients_menu_text = self.selenium.find_element_by_id('clients_navbar').text
        # We have the element text so now we check if it is in fact "Clients"
        self.assertIn('Clients', clients_menu_text)
        # Now we've tested that the login fails with wrong credentials, and succeeds with correct ones!

    def test_create_client(self):
        self.login('betty', 'changeme')
        # Click on clients
        self.selenium.find_element_by_id('clients_navbar').click()
        self.selenium.find_element_by_id('create_btn').click()
        # Fill out the create client form
        self.selenium.find_element_by_name("name").send_keys('Test Company')
        self.selenium.find_element_by_name("address").send_keys('Test Address')
        self.selenium.find_element_by_name("contact_name").send_keys('Test Contact')
        self.selenium.find_element_by_name("contact_email").send_keys('testcontactemail@testcompany.com')
        # Hit submit/create on that form
        self.selenium.find_element_by_id('create_btn').click()
        # Check that the new client was created by making sure it is on the client list!
        client_names = self.selenium.find_elements_by_id('name')
        self.assertTrue('Test Company' in [client.text for client in client_names])
        client_contacts = self.selenium.find_elements_by_id('contact_name')
        self.assertTrue('Test Contact' in [client.text for client in client_contacts])
        client_emails = self.selenium.find_elements_by_id('contact_email')
        self.assertTrue('testcontactemail@testcompany.com' in [client.text for client in client_emails])
        client_states = self.selenium.find_elements_by_id('contact_notifications')
        self.assertTrue('True' in [client.text for client in client_states])

    def test_create_teacher(self):
        self.login('betty', 'changeme')
        # Click on teachers
        self.selenium.find_element_by_id('teachers_navbar').click()
        # Click on Create Teacher button
        self.selenium.find_element_by_id('create_btn').click()
        # Fill out the create teacher form
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('test_teacher')
        first_name_input = self.selenium.find_element_by_name("first_name")
        last_name_input = self.selenium.find_element_by_name("last_name")
        first_name_input.send_keys('Test')
        last_name_input.send_keys('Teacher')
        email_input = self.selenium.find_element_by_name("email")
        email_input.send_keys('test_teacher@betty.com')
        phone_input = self.selenium.find_element_by_name("phone")
        phone_input.send_keys('1234567')
        self.selenium.find_element_by_name("country").send_keys('Colombia')
        self.selenium.find_element_by_name("specialty").send_keys('Quantum Physics')
        # Click create teacher button
        self.selenium.find_element_by_id('create_btn').click()
        # Check that the new teacher was created by making sure it is on the teacher list
        teacher_names = self.selenium.find_elements_by_id('name')
        self.assertTrue('Test Teacher' in [teacher.text for teacher in teacher_names])
        teacher_usernames = self.selenium.find_elements_by_id('username')
        self.assertTrue('test_teacher' in [teacher.text for teacher in teacher_usernames])
        teacher_emails = self.selenium.find_elements_by_id('email')
        self.assertTrue('test_teacher@betty.com' in [teacher.text for teacher in teacher_emails])
        teacher_phones = self.selenium.find_elements_by_id('phone')
        self.assertTrue('1234567' in [teacher.text for teacher in teacher_phones])
        teacher_countries = self.selenium.find_elements_by_id('country')
        self.assertTrue('Colombia' in [teacher.text for teacher in teacher_countries])
        teacher_specialties = self.selenium.find_elements_by_id('specialty')
        self.assertTrue('Quantum Physics' in [teacher.text for teacher in teacher_specialties])

    def test_create_student(self):
        self.login('betty', 'changeme')
        # Click on Students
        self.selenium.find_element_by_id('students_navbar').click()
        # Click on Create Student button
        self.selenium.find_element_by_id('top_create_btn').click()
        # Fill out the create Student form
        self.selenium.find_element_by_name("name").send_keys('Test Student')
        self.selenium.find_element_by_name("email").send_keys('test_student@betty.com')
        self.selenium.find_element_by_name("phone").send_keys('1234567')
        self.selenium.find_element_by_xpath('//*[@id="id_client"]/option[text()="BettyCoClient1"]').click()
        # Click create Student button
        self.selenium.find_element_by_id('create_btn').click()
        # Check that the new Student was created by making sure it is on the student list
        student_names = self.selenium.find_elements_by_id('name')
        self.assertTrue('Test Student' in [student.text for student in student_names])
        student_emails = self.selenium.find_elements_by_id('email')
        self.assertTrue('test_student@betty.com' in [student.text for student in student_emails])
        student_phones = self.selenium.find_elements_by_id('phone')
        self.assertTrue('1234567' in [student.text for student in student_phones])
        student_clients = self.selenium.find_elements_by_id('client')
        self.assertTrue('BettyCoClient1' in [student.text for student in student_clients])

    def test_create_course(self):
        self.login('betty', 'changeme')
        # Click on Courses
        self.selenium.find_element_by_id('courses_navbar').click()
        # Click on Create Course button
        self.selenium.find_element_by_id('create_btn').click()
        # Fill out the create Course form
        self.selenium.find_element_by_xpath('//*[@id="id_level"]/option[text()="Advanced"]').click()
        self.selenium.find_element_by_xpath('//*[@id="id_client"]/option[text()="BettyCoClient1"]').click()
        self.selenium.find_element_by_name('address').send_keys('Course Address')
        self.selenium.find_element_by_name('schedule').send_keys('Course Schedule')
        self.selenium.find_element_by_name('start_date').send_keys('2017-02-01')
        self.selenium.find_element_by_name('end_date').send_keys('2017-06-30')
        # Select students in multiselect dropdown
        self.selenium.find_element_by_css_selector('#content > form > div:nth-child(10) > span > div > button').click()
        self.selenium.find_element_by_css_selector(
            '#content > form > div:nth-child(10) > span > div > ul > li:nth-child(2) > a > label > input[type="checkbox"]'
        ).click()
        self.selenium.find_element_by_css_selector(
            '#content > form > div:nth-child(10) > span > div > ul > li:nth-child(3) > a > label > input[type="checkbox"]'
        ).click()
        self.selenium.find_element_by_xpath('//*[@id="id_teacher"]/option[text()="Betty Teacher 1"]').click()
        # Click on Create Course button
        self.selenium.find_element_by_id('create_btn').click()
        # Check each row in Detail view to confirmate course info
        course_code = self.selenium.find_element_by_id('code').text
        self.assertTrue('BETTYCOCLIENT1-1', course_code)
        course_level = self.selenium.find_element_by_id('level').text
        self.assertTrue('Advanced', course_level)
        course_client = self.selenium.find_element_by_id('client').text
        self.assertTrue('BettyCoClient1', course_client)
        course_address = self.selenium.find_element_by_id('address').text
        self.assertTrue('Course Address', course_address)
        course_schedule = self.selenium.find_element_by_id('schedule').text
        self.assertTrue('Course Schedule', course_schedule)
        course_start = self.selenium.find_element_by_id('start_date').text
        self.assertTrue('Feb. 28, 2017', course_start)
        course_end = self.selenium.find_element_by_id('end_date').text
        self.assertTrue('March 28, 2017', course_end)
        course_teacher = self.selenium.find_element_by_id('teacher').text
        self.assertTrue('Betty Teacher 1', course_teacher)
        course_duration = self.selenium.find_element_by_id('duration').text
        self.assertTrue('48', course_duration)

    def test_update_client(self):
        self.login('betty', 'changeme')
        # Click on clients
        self.selenium.find_element_by_id('clients_navbar').click()
        # Click on Client Details button
        self.selenium.find_element_by_xpath("(//*[@id='details_btn'])[1]").click()
        # Click on Edit Client button
        self.selenium.find_element_by_id('edit_btn').click()
        # Change data on update form
        self.selenium.find_element_by_name("name").clear()
        self.selenium.find_element_by_name("name").send_keys('BettyClient')
        self.selenium.find_element_by_name("address").clear()
        self.selenium.find_element_by_name("address").send_keys('Other address')
        self.selenium.find_element_by_name("contact_name").clear()
        self.selenium.find_element_by_name("contact_name").send_keys('Other contact')
        self.selenium.find_element_by_name("contact_email").clear()
        self.selenium.find_element_by_name("contact_email").send_keys('otheremail@othercontact.com')
        self.selenium.find_element_by_id('update_btn').click()
        # Check new info in Client Details View
        client_new_name = self.selenium.find_element_by_id('title').text
        self.assertTrue('Client Details - BettyClient', client_new_name)
        client_new_address = self.selenium.find_element_by_id('address').text
        self.assertTrue('Other Address', client_new_address)
        client_new_contact = self.selenium.find_element_by_id('contact_name').text
        self.assertTrue('Other Contact', client_new_contact)
        client_new_contact_email = self.selenium.find_element_by_id('email').text
        self.assertTrue('otheremail@othercontact.com', client_new_contact_email)

    def test_update_teacher(self):
        self.login('betty', 'changeme')
        # Click on Teachers
        self.selenium.find_element_by_id('teachers_navbar').click()
        # Click on Teacher Details button
        self.selenium.find_element_by_xpath("(//*[@id='details_btn'])[1]").click()
        # Click on Edit Teacher
        self.selenium.find_element_by_id('edit_btn').click()
        # Change data on update form
        self.selenium.find_element_by_name("name").clear()
        self.selenium.find_element_by_name("name").send_keys('teacher 1')
        self.selenium.find_element_by_name("email").clear()
        self.selenium.find_element_by_name("email").send_keys('otheremail@betty.com')
        self.selenium.find_element_by_name("phone").clear()
        self.selenium.find_element_by_name("phone").send_keys('9876543')
        self.selenium.find_element_by_name("country").clear()
        self.selenium.find_element_by_name("country").send_keys('Colombia')
        self.selenium.find_element_by_name("specialty").clear()
        self.selenium.find_element_by_name("specialty").send_keys('Quantum Physics')
        # Click on Update Teacher button
        self.selenium.find_element_by_id('update_btn').click()
        # Check new info in Teacher Details View
        teacher_new_name = self.selenium.find_element_by_id('name').text
        self.assertTrue('teacher 1', teacher_new_name)
        teacher_new_email = self.selenium.find_element_by_css_selector('.teacher_email').text
        self.assertTrue('otheremail@betty.com', teacher_new_email)
        teacher_new_phone = self.selenium.find_element_by_id('phone').text
        self.assertTrue('9876543', teacher_new_phone)
        teacher_new_country = self.selenium.find_element_by_id('country').text
        self.assertTrue('Colombia', teacher_new_country)
        teacher_new_specialty = self.selenium.find_element_by_id('specialty').text
        self.assertTrue('Quantum Physics', teacher_new_specialty)

    def test_update_student(self):
        self.login('betty', 'changeme')
        # Click on Students
        self.selenium.find_element_by_id('students_navbar').click()
        # Click on Student Details button
        self.selenium.find_element_by_xpath("(//*[@id='details_btn'])[1]").click()
        # Click on Edit Student button
        self.selenium.find_element_by_id('edit_btn').click()
        # Change data in update form
        self.selenium.find_element_by_name("name").clear()
        self.selenium.find_element_by_name("name").send_keys('Other betty student')
        self.selenium.find_element_by_name("email").clear()
        self.selenium.find_element_by_name("email").send_keys('otherstudentemail@betty.com')
        self.selenium.find_element_by_name("phone").clear()
        self.selenium.find_element_by_name("phone").send_keys('7654321')
        # Click on Update Student button
        self.selenium.find_element_by_id('update_btn').click()
        # Check new info in Student Details View
        student_new_name = self.selenium.find_element_by_id('name').text
        self.assertTrue('Other betty student', student_new_name)
        student_new_email = self.selenium.find_element_by_id('email').text
        self.assertTrue('otherstudentemail@betty.com', student_new_email)
        student_new_phone = self.selenium.find_element_by_id('phone').text
        self.assertTrue('7654321', student_new_phone)

    def test_update_course(self):
        self.login('betty', 'changeme')
        # Click on Courses
        self.selenium.find_element_by_id('courses_navbar').click()
        # Click on View Course Button
        self.selenium.find_element_by_xpath("(//*[@id='details_btn'])[1]").click()
        # Click on Edit Course button
        self.selenium.find_element_by_id('edit_btn').click()
        # Change data in edit form
        self.selenium.find_element_by_name('address').clear()
        self.selenium.find_element_by_name('address').send_keys('Other Address')
        self.selenium.find_element_by_name('schedule').clear()
        self.selenium.find_element_by_name('schedule').send_keys('Other Schedule')
        self.selenium.find_element_by_css_selector(
            '#content > form > div.center-block.btn-group-vertical > input'
        ).click()
        course_address = self.selenium.find_element_by_id('address').text
        self.assertTrue('Other Address', course_address)
        course_schedule = self.selenium.find_element_by_id('schedule').text
        self.assertTrue('Other Schedule', course_schedule)

    def test_delete_teacher(self):
        self.login('betty', 'changeme')
        # Click on teachers
        self.selenium.find_element_by_id('teachers_navbar').click()
        teacher_to_delete_username = self.selenium.find_element_by_xpath("(//*[@id='username'])[1]").text
        teacher_to_delete_email = self.selenium.find_element_by_xpath("(//*[@id='email'])[1]").text
        # Click on Teacher Details button
        self.selenium.find_element_by_xpath("(//*[@id='details_btn'])[1]").click()
        # Find teacher in db
        email = self.selenium.find_element_by_css_selector(".teacher_email").text
        teacher_to_delete_id = Teacher.objects.get(email=email).id
        # Save teacher is_active value
        teacher_to_delete_status = Teacher.objects.get(pk=teacher_to_delete_id).user.is_active
        # Check if teacher is an active user
        self.assertTrue(teacher_to_delete_status, True)
        # Click on delete button
        self.selenium.find_element_by_id('delete_btn').click()
        # Click on confirmation button
        self.selenium.find_element_by_id('confirm_btn').click()
        first_teacher_username = self.selenium.find_element_by_xpath("(//*[@id='username'])[1]").text
        first_teacher_email = self.selenium.find_element_by_xpath("(//*[@id='email'])[1]").text
        try:
            teacher_to_delete_status = Teacher.objects.get(pk=teacher_to_delete_id).user.is_active
        except:
            teacher_to_delete_status = False
        # Check if teacher is an inactive user
        self.assertEqual(teacher_to_delete_status, False)
        # Check the fields that can not be repeated to verify that the teacher was deleted
        self.assertNotEqual(first_teacher_username, teacher_to_delete_username)
        self.assertNotEqual(first_teacher_email, teacher_to_delete_email)

    def test_delete_student(self):
        self.login('betty', 'changeme')
        # Click on Students
        self.selenium.find_element_by_id('students_navbar').click()
        student_to_delete_email = self.selenium.find_element_by_xpath("(//*[@id='email'])[1]").text
        # Click on Student Details button
        self.selenium.find_element_by_xpath("(//*[@id='details_btn'])[1]").click()
        # Click on delete button
        self.selenium.find_element_by_id('delete_btn').click()
        # Click on confirmation button
        self.selenium.find_element_by_id('confirm_btn').click()
        first_student_email = self.selenium.find_element_by_xpath("(//*[@id='email'])[1]").text
        # Check the fields that can not be repeated to verify that the student was deleted
        self.assertNotEqual(first_student_email, student_to_delete_email)

    def test_delete_client(self):
        self.login('betty', 'changeme')
        # Click on clients
        self.selenium.find_element_by_id('clients_navbar').click()
        client_to_delete_name = self.selenium.find_element_by_xpath("(//*[@id='name'])[1]").text
        client_to_delete_email = self.selenium.find_element_by_xpath("(//*[@id='contact_email'])[1]").text
        # Click on Client Details button
        self.selenium.find_element_by_xpath("(//*[@id='details_btn'])[1]").click()
        # Click on delete button
        self.selenium.find_element_by_id('delete_btn').click()
        # Click on confirmation button
        self.selenium.find_element_by_id('confirm_btn').click()
        first_client_name = self.selenium.find_element_by_xpath("(//*[@id='name'])[1]").text
        first_client_email = self.selenium.find_element_by_xpath("(//*[@id='contact_email'])[1]").text
        # Check the fields that can not be repeated to verify that the client was deleted
        self.assertNotEqual(first_client_name, client_to_delete_name)
        self.assertNotEqual(first_client_email, client_to_delete_email)

    def test_delete_course(self):
        self.login('betty', 'changeme')
        # Click on Courses
        self.selenium.find_element_by_id('courses_navbar').click()
        course_to_delete = self.selenium.find_element_by_xpath("(//*[@id='code'])[1]").text
        # Click on View Course Button
        self.selenium.find_element_by_xpath("(//*[@id='details_btn'])[1]").click()
        # Click Delete Course button
        self.selenium.find_element_by_id('delete_btn').click()
        # Click Confirmation button
        self.selenium.find_element_by_id('confirm_btn').click()
        # Check the fields that can not be repeated to verify that the course was deleted
        first_course = self.selenium.find_element_by_xpath("(//*[@id='code'])[1]").text
        self.assertNotEqual(first_course, course_to_delete)

    def test_delete_period_course(self):
        self.test_create_course()
        period_to_delete = self.selenium.find_element_by_class_name('period_title').text
        self.selenium.find_element_by_class_name('delete_period_btn').click()
        self.selenium.find_element_by_id('confirm_btn').click()
        first_period = self.selenium.find_element_by_class_name('period_title').text
        self.assertNotEqual(period_to_delete, first_period)

    def test_create_period_course(self):
        self.test_create_course()
        self.selenium.find_element_by_class_name('add_period_btn').click()
        self.selenium.find_element_by_name('start_date').send_keys('2017-01-01')
        self.selenium.find_element_by_name('end_date').send_keys('2017-02-28')
        self.selenium.find_element_by_class_name('create_btn').click()
        error_list = self.selenium.find_element_by_class_name('errorlist').text
        self.assertIn(
            'Start date cannot be before the start of the course, which is on 2017-02-01.',
            error_list
        )
        self.selenium.find_element_by_name('start_date').clear()
        self.selenium.find_element_by_name('start_date').send_keys('2017-04-01')
        self.selenium.find_element_by_name('end_date').clear()
        self.selenium.find_element_by_name('end_date').send_keys('2017-08-28')
        self.selenium.find_element_by_class_name('create_btn').click()
        WebDriverWait(WebDriver(executable_path=settings.CHROMEDRIVER_PATH), 5)
        error_list = self.selenium.find_element_by_class_name('errorlist').text
        self.assertIn(
            'End date cannot be after the end of the course, which is on 2017-06-30.',
            error_list
        )
        self.selenium.find_element_by_name('start_date').clear()
        self.selenium.find_element_by_name('start_date').send_keys('2017-04-15')
        self.selenium.find_element_by_name('end_date').clear()
        self.selenium.find_element_by_name('end_date').send_keys('2017-04-01')
        self.selenium.find_element_by_class_name('create_btn').click()
        WebDriverWait(WebDriver(executable_path=settings.CHROMEDRIVER_PATH), 5)
        error_list = self.selenium.find_element_by_class_name('errorlist').text
        self.assertIn(
            'Start date cannot be after the end date.',
            error_list
        )
        self.selenium.find_element_by_name('start_date').clear()
        self.selenium.find_element_by_name('start_date').send_keys('2017-04-15')
        self.selenium.find_element_by_name('end_date').clear()
        self.selenium.find_element_by_name('end_date').send_keys('2017-05-15')
        self.selenium.find_element_by_class_name('create_btn').click()
        period_titles = self.selenium.find_elements_by_class_name('period_title')
        self.assertTrue('Course Skills for Period Apr 15, 2017 to May 15, 2017'
                        in [period_title.text for period_title in period_titles])
