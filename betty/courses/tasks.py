# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from courses.models import Course, CoursePeriod
from reports.reports import trigger_student_report, trigger_client_report, trigger_manager_report, \
    trigger_course_incomplete_report, trigger_course_start_report


@shared_task
def send_end_of_period_reports(course_id, period_id, incomplete=False):
    course = Course.objects.get(pk=course_id)
    period = CoursePeriod.objects.get(pk=period_id)

    if incomplete:
        trigger_course_incomplete_report(course.id, period.id)
        return

    for student in course.students.all():
        trigger_student_report(course, student, period)

    # Send reports to the client company for the course about the ending period.
    trigger_client_report(course, period)
    # Send manager reports to the school admins.
    trigger_manager_report(course, period)


@shared_task
def send_course_welcome_reports(course_id):
    trigger_course_start_report(course_id)


@shared_task
def _raise_error():
    0/0

