# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-13 01:40
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0011_auto_20170412_0300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clientcompany',
            name='sales_associates',
            field=models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='student',
            name='sales_associates',
            field=models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
