from betty import settings
from courses.models import Course, ClientCompany, Student, Teacher, Activity, CompanySettings, CoursePeriod, \
    CompanyActivity
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.transaction import atomic
from users.models import Company


class CourseCreationForm(forms.ModelForm):

    template_name = 'courses/course_create.html'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(CourseCreationForm, self).__init__(*args, **kwargs)
        self.fields['client'] = forms.ModelChoiceField(
            queryset=ClientCompany.objects.filter(company=self.user.userprofile.company_id)
        )
        self.fields['students'] = forms.ModelMultipleChoiceField(
            queryset=Student.objects.filter(company=self.user.userprofile.company_id),
            widget=forms.SelectMultiple(attrs={'class': 'multiselect_field'})
        )
        self.fields['teacher'] = forms.ModelChoiceField(
            queryset=Teacher.objects.filter(company=self.user.userprofile.company_id)
        )

    def clean_students(self):
        for student in self.cleaned_data['students']:
            # Use is and is not for singletons like None - that checks for identity, here we need to check equality
            if student.client.id != self.cleaned_data['client'].id:
                raise ValidationError('The student %s, does not belong to the client %s.'
                                      % (student.name, self.cleaned_data['client']))
        return self.cleaned_data['students']

    class Meta:
        model = Course
        fields = ['level', 'client', 'address', 'schedule', 'start_date', 'end_date', 'students', 'teacher', 'duration']
        widgets = {
            'start_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'end_date': forms.DateInput(attrs={'class': 'datepicker'}),
        }


class CourseUpdateForm(forms.ModelForm):

    template_name = 'courses/course_create.html'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(CourseUpdateForm, self).__init__(*args, **kwargs)
        self.fields['client'] = forms.ModelChoiceField(
            queryset=ClientCompany.objects.filter(company=self.user.userprofile.company_id)
        )
        self.fields['students'] = forms.ModelMultipleChoiceField(
            queryset=Student.objects.filter(company=self.user.userprofile.company_id),
            widget=forms.SelectMultiple(attrs={'class': 'multiselect_field'})
        )
        self.fields['teacher'] = forms.ModelChoiceField(
            queryset=Teacher.objects.filter(company=self.user.userprofile.company_id)
        )

    def clean_students(self):
        for student in self.cleaned_data['students']:
            # Use is and is not for singletons like None - that checks for identity, here we need to check equality
            if student.client.id != self.cleaned_data['client'].id:
                raise ValidationError('The student %s, does not belong to the client %s.'
                                      % (student.name, self.cleaned_data['client']))
        return self.cleaned_data['students']

    class Meta:
        model = Course
        fields = ['level', 'client', 'address', 'schedule', 'students', 'teacher', 'duration', 'start_date', 'end_date']


class StudentCreationForm(forms.ModelForm):

    def clean_name(self):
        if 'name' not in self.cleaned_data:
            raise forms.ValidationError("Student needs to have a name.")

        return self.cleaned_data['name']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(StudentCreationForm, self).__init__(*args, **kwargs)
        self.fields['client'] = forms.ModelChoiceField(
            queryset=ClientCompany.objects.filter(company=self.user.userprofile.company_id)
        )
        self.fields['sales_associates'] = forms.ModelMultipleChoiceField(
            queryset=User.objects.filter(userprofile__company=self.user.userprofile.company_id, userprofile__roles__name='sales'),
            widget=forms.SelectMultiple(attrs={'required': False}),
            required=False
        )

    template_name = 'courses/student_create.html'

    class Meta:
        model = Student
        fields = ['name', 'email', 'phone', 'client', 'sales_associates']

    def create_student(self, company_id):
        student = Student.objects.create_student(self.cleaned_data, company_id)
        return student


class StudentListFiltrationForm(forms.Form):

    template_name = 'courses/student_list.html'
    student_name = forms.CharField(max_length=20, required=False)
    student_email = forms.CharField(max_length=20, required=False)
    student_phone = forms.CharField(max_length=20, required=False)
    client = forms.CharField(max_length=20, required=False)

    def __init__(self, *args, **kwargs):
        self.student_name = kwargs.pop('student_name')
        self.student_email = kwargs.pop('student_email')
        self.student_phone = kwargs.pop('student_phone')
        self.client = kwargs.pop('client')
        super(StudentListFiltrationForm, self).__init__(*args, **kwargs)


class TeacherCreationForm(forms.ModelForm):

    template_name = 'courses/teacher_create.html'
    username = forms.CharField(max_length=30)
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    country = forms.CharField(max_length=50)
    specialty = forms.Textarea()

    class Meta:
        model = Teacher
        fields = ['username', 'first_name', 'last_name', 'email', 'phone', 'country', 'specialty']

    def create_teacher(self, company_id):
        teacher = Teacher.objects.create_teacher(self.cleaned_data, company_id)
        return teacher

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError("The selected username is not available.")

        return username


class CoursePeriodCreationForm(forms.ModelForm):

    template_name = 'courses/period_create.html'

    course = None

    class Meta:
        model = CoursePeriod
        fields = ['start_date', 'end_date']

    def __init__(self, *args, **kwargs):
        self.course = Course.objects.get(pk=kwargs.pop('course_id'))
        super(CoursePeriodCreationForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(CoursePeriodCreationForm, self).clean()
        start_date = cleaned_data.get("start_date")
        end_date = cleaned_data.get("end_date")

        if start_date > end_date:
            raise ValidationError('Start date cannot be after the end date.')

        if start_date < self.course.start_date:
            raise ValidationError('Start date cannot be before the start of the course, which is on {0}.'.format(self.course.start_date))

        if end_date > self.course.end_date:
            raise ValidationError('End date cannot be after the end of the course, which is on {0}.'.format(self.course.end_date))

    def create_period(self, course_id):
        period = CoursePeriod.objects.create(course=self.course, is_active=True, **self.cleaned_data)
        company_activities = list(CompanyActivity.objects.filter(company_id=self.course.company_id).values('name', 'type'))
        for student in self.course.students.all():
            for activity in company_activities:
                if activity['type'] == 'skill' or activity['type'] == 'period_feedback':
                    Activity.objects.create(
                        company=self.course.company,
                        course=self.course,
                        student=student,
                        name=activity['name'],
                        type=activity['type'],
                        period=period
                    )

        return period


class ClientCompanyCreationForm(forms.ModelForm):

    template_name = 'courses/client_create.html'
    user = None

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(ClientCompanyCreationForm, self).__init__(*args, **kwargs)
        self.fields['sales_associates'] = forms.ModelMultipleChoiceField(
            queryset=User.objects.filter(userprofile__company=self.user.userprofile.company_id, userprofile__roles__name='sales'),
            widget=forms.SelectMultiple(),
            required=False
        )

    class Meta:
        model = ClientCompany
        fields = ['name', 'address', 'contact_name', 'contact_email', 'notifications_active', 'sales_associates']

    def create_client(self, creator):
        new_client = ClientCompany.objects.create(
            name=self.cleaned_data['name'],
            address=self.cleaned_data['address'],
            contact_name=self.cleaned_data['contact_name'],
            contact_email=self.cleaned_data['contact_email'],
            company=creator.userprofile.company
        )
        new_client.sales_associates = self.cleaned_data['sales_associates']
        new_client.save()
        return new_client


class ActivityUpdateForm(forms.ModelForm):

    template_name = 'courses/activity_update.html'
    comment = forms.Textarea(attrs={'required': False})
    score = forms.NumberInput(attrs={'required': False})

    def __init__(self, *args, **kwargs):
        self.course = kwargs.pop('course')
        super(ActivityUpdateForm, self).__init__(*args, **kwargs)

    def clean_score(self):
        self._validate_activity()
        return self.cleaned_data['score']

    def _validate_activity(self):

        try:
            max_score = float(CompanySettings.objects.get(company=self.instance.course.company_id, name='test_max_score').value)
        except:
            max_score = settings.DEFAULT_MAX_SCORE

        try:
            max_skill_score = float(CompanySettings.objects.get(company=self.instance.course.company_id, name='skill_max_score').value)
        except:
            max_skill_score = settings.DEFAULT_MAX_SCORE

        if self.instance.type not in ['feedback', 'period_feedback']:
            if 'score' not in self.cleaned_data or self.cleaned_data['score'] is None:
                raise forms.ValidationError("A score is required.")

            # @todo:gustavo attendance figure out what attendance is scored like, for now as a skill on both period and end of course.
            if self.instance.type == 'test':
                if self.instance.name == 'Attendance':
                    if self.cleaned_data['score'] > 100 or self.cleaned_data['score'] < 0:
                        raise forms.ValidationError("Attendance has to be between 0 and 100.")
                if self.cleaned_data['score'] > max_score or self.cleaned_data['score'] < 0:
                    raise forms.ValidationError("Score has to be between 0 and %s." % max_score)

            if self.instance.type == 'skill':
                # @todo:gustavo attendance figure out what attendance is scored like, for now as a skill on both period and end of course.
                # if self.instance.name == 'Attendance':
                #     if self.cleaned_data['score'] > self.instance.course.duration or \
                #                     self.cleaned_data['score'] < 0:
                #         raise forms.ValidationError("Attendance has to be a value for hours between 0 and %s." % self.instance.course.duration)
                if self.cleaned_data['score'] > max_skill_score or self.cleaned_data['score'] < 0:
                    raise forms.ValidationError("Score has to be between 0 and %s." % max_skill_score)


    class Meta:
        model = Activity
        fields = ['score', 'comment']


class ActivityCreationForm(forms.ModelForm):

    template_name = 'courses/activity_create.html'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.course_id = kwargs.pop('course')
        super(ActivityCreationForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Activity
        fields = ['name', 'type']

    @atomic
    def create_activity(self, creator):
        """
        Creates an activity for each student enroled in the current course
        :param creator:
        :return:
        """
        students = Student.objects.filter(course=self.course_id)
        for student in students:
            new_activity = Activity.objects.create(
                name=self.cleaned_data['name'],
                type=self.cleaned_data['type'],
                student=student,
                course_id=self.course_id,
                company=creator.userprofile.company
            )