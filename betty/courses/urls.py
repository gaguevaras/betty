from courses.views import CourseCreationView, CoursesListView
from django.conf.urls import url

from . import views

urlpatterns = [
    # Courses
    url(r'^create/$', views.CourseCreationView.as_view(), name='course_create'),
    url(r'^edit/(?P<pk>[0-9]+)/$', views.CourseUpdateView.as_view(), name='course_update'),
    url(r'^list/$', views.CoursesListView.as_view(), name='course_list'),
    url(r'^detail/(?P<pk>[0-9]+)/$', views.CourseDetailView.as_view(), name='course_detail'),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.CourseDeleteView.as_view(), name='course_delete'),

    #Period
    url(r'^period/edit/(?P<pk>[0-9]+)/$', views.CoursePeriodUpdateView.as_view(), name='period_update'),
    url(r'^period/delete/(?P<pk>[0-9]+)/$', views.CoursePeriodDeleteView.as_view(), name='period_delete'),
    url(r'^detail/(?P<course_id>[0-9]+)/period/create/$', views.CoursePeriodCreationView.as_view(), name='period_create'),

    url(r'^student/create/$', views.StudentCreationView.as_view(), name='student_create'),
    url(r'^student/multiple/(?P<amount>[0-9]+)/$', views.manage_students, name='multiple_student_create'),
    url(r'^student/edit/(?P<pk>[0-9]+)/$', views.StudentUpdateView.as_view(), name='student_update'),
    url(r'^student/list/$', views.StudentsListView.as_view(), name='student_list'),
    url(r'^student/detail/(?P<pk>[0-9]+)/$', views.StudentDetailView.as_view(), name='student_detail'),
    url(r'^student/delete/(?P<pk>[0-9]+)/$', views.StudentDeleteView.as_view(), name='student_delete'),

    url(r'^teacher/create/$', views.TeacherCreationView.as_view(), name='teacher_create'),
    url(r'^teacher/edit/(?P<pk>[0-9]+)/$', views.TeacherUpdateView.as_view(), name='teacher_update'),
    url(r'^teacher/list/$', views.TeachersListView.as_view(), name='teacher_list'),
    url(r'^teacher/detail/(?P<pk>[0-9]+)/$', views.TeacherDetailView.as_view(), name='teacher_detail'),
    url(r'^teacher/delete/(?P<pk>[0-9]+)/$', views.TeacherDeleteView.as_view(), name='teacher_delete'),

    url(r'^client/create/$', views.ClientCompanyCreationView.as_view(), name='client_create'),
    url(r'^client/edit/(?P<pk>[0-9]+)/$', views.ClientCompanyUpdateView.as_view(), name='client_update'),
    url(r'^client/list/$', views.ClientCompanyListView.as_view(), name='client_list'),
    url(r'^client/detail/(?P<pk>[0-9]+)/$', views.ClientCompanyDetailView.as_view(), name='client_detail'),
    url(r'^client/delete/(?P<pk>[0-9]+)/$', views.ClientCompanyDeleteView.as_view(), name='client_delete'),

    url(r'^detail/(?P<course_id>[0-9]+)/activity/create/$', views.ActivityCreationView.as_view(), name='activity_create'),
    url(r'^detail/(?P<course_id>[0-9]+)/activity/edit/(?P<pk>[0-9]+)/$', views.ActivityUpdateView.as_view(), name='activity_update'),
    url(r'^activity/list/$', views.ActivityListView.as_view(), name='activity_list'),
    url(r'^activity/detail/(?P<pk>[0-9]+)/$', views.ActivityDetailView.as_view(), name='activity_detail'),
    url(r'^activity/delete/(?P<pk>[0-9]+)/$', views.ActivityDeleteView.as_view(), name='activity_delete'),

    url(r'^detail/(?P<course_id>[0-9]+)/end/$', views.end_course, name='end_course'),
    url(r'^detail/(?P<course_id>[0-9]+)/start/$', views.start_course, name='start_course'),
    url(r'^detail/(?P<course_id>[0-9]+)/end_period/(?P<period_id>[0-9]+)$', views.end_period, name='end_period'),
    url(r'^raise_error/$', views.raise_error, name='raise_error'),
    url(r'^raise_error_delay/$', views.raise_error_delay, name='raise_error_delay'),

]
