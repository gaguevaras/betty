import json

from django.http import HttpResponse


class AjaxFormResponseMixin(object):

    def form_invalid(self, form):
        response = {
            'errors': form.errors,
            'old_value': self.get_context_data()['activity'].score
        }
        return render_to_json_response(response, status=400)

    def form_valid(self, form):
        object = form.save()
        json_response = self.get_serializable_data()
        json_response['message'] = 'Score saved for %s.' % object
        return render_to_json_response(json_response)


def render_to_json_response(context, **response_kwargs):
    # returns a JSON response, transforming 'context' to make the payload
    response_kwargs['content_type'] = 'application/json'
    return HttpResponse(convert_context_to_json(context), **response_kwargs)


def convert_context_to_json(context):
    # convert the context dictionary into a JSON object
    # note: this is *EXTREMELY* naive; in reality, you'll need
    # to do much more complex handling to ensure that arbitrary
    # objects -- such as Django model instances or querysets
    # -- can be serialized as JSON.
    return json.dumps(context)
