from betty.decorators import role_required
from courses.models import CompanySettings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import authenticate, login

# Create your views here.
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.transaction import atomic
from django.utils.decorators import method_decorator
from registration import signals
from registration.forms import User
from registration.views import RegistrationView
from users.forms import CompanyRegistrationForm, UserRegistrationForm
from users.models import UserManager, UserRole


@method_decorator(staff_member_required, name='dispatch')
class BettyCompanyRegistrationView(RegistrationView):

    """
    Registration via the simplest possible process: a user supplies a
    username, email address and password (the bare minimum for a
    useful account), and is immediately signed up and logged in).

    """

    form_class = CompanyRegistrationForm

    @atomic
    def register(self, form):

        user_manager = UserManager()
        new_user = form.save()
        # Create a company and profile for the new user
        new_company = user_manager.create_company(form.cleaned_data)
        new_profile = user_manager.create_profile(new_user, new_company, [UserRole.objects.get(name='admin')])

        # Copy Company settings from Betty
        betty_settings = CompanySettings.objects.filter(company_id=1)
        for setting in betty_settings:
            setting.pk = None
            setting.company = new_company
            setting.save()

        # Log the user in
        new_user = authenticate(
            username=getattr(new_user, User.USERNAME_FIELD),
            password=form.cleaned_data['password1']
        )
        login(self.request, new_user)
        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=self.request)
        return new_user

    def get_success_url(self, user):
        return '/'


class UserCreationView(LoginRequiredMixin, UserPassesTestMixin, RegistrationView):

    form_class = UserRegistrationForm
    authorized_roles = ['admin']

    def test_func(self):
        if self.request.user.is_authenticated():
            if bool(self.request.user.userprofile.roles.filter(name__in=self.authorized_roles).count()) | self.request.user.is_superuser:
                return True
        return False

    @atomic
    def register(self, form):

        new_user = form.save()
        new_user.first_name = form.cleaned_data['first_name']
        new_user.last_name = form.cleaned_data['last_name']
        new_user.save()
        user_manager = UserManager()
        user_manager.create_profile(
            new_user,
            self.request.user.userprofile.company,
            [UserRole.objects.get(id=form.cleaned_data['role'])]
        )

        return new_user

    def get_success_url(self, user):
        return '/'

