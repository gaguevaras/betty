from django.test import TestCase, Client


# Create your tests here.
from users.models import Company


class CompanyTests(TestCase):

    fixtures = ['db.json']

    def setUp(self):
        self.client = Client()

    def test_company_creation_form_is_accesible_only_by_admin(self):
        self.client.login(username='superadmin', password='test_password')
        response = self.client.get('/accounts/register/')
        self.assertEqual(response.status_code, 200)
        self.client.login(username='betty', password='changeme')
        response = self.client.get('/accounts/register/')
        self.assertEqual(response.status_code, 302)
        # @todo:gustavo this test is not correct - the assertion is not indicative of what is being tested - FIXME

    def test_company_creation(self):
        self.client.login(username='superadmin', password='test_password')
        company_data = {
            'username': 'test_user',
            'email': 'test@betty.com',
            'password1': 'test_password',
            'password2': 'test_password',
            'first_name': 'first_name',
            'last_name': 'last_name',
            'company_name': 'company1',
            'address': 'some address'
        }
        response = self.client.post('/accounts/register/', company_data, follow=True)
        self.assertEqual(response.status_code, 200)
        company = Company.objects.get(name=company_data['company_name'], contact_email=company_data['email'])
        self.assertNotEquals(company, None)

