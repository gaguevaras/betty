from django import forms
from django.contrib.auth.models import User
from registration.forms import RegistrationForm
from users.models import UserRole


class CompanyRegistrationForm(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which adds the fields necessary to register a company.

    """
    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    company_name = forms.CharField(label='Company Name', max_length=100)
    address = forms.CharField(max_length=200)


class UserRegistrationForm(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which adds the fields necessary to register a company.

    """
    def __init__(self, *args, **kwargs):
        super(UserRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['role'] = forms.ChoiceField(
            choices=[(o.id, str(o)) for o in UserRole.objects.filter(name__in=['manager', 'admin', 'sales'])]
        )

    first_name = forms.CharField(max_length=100)
    last_name = forms.CharField(max_length=100)
    role = forms.ChoiceField()


