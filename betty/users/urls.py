from courses.views import CourseCreationView, CoursesListView
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^create/$', views.UserCreationView.as_view(), name='user_create'),
]
