from django import template
register = template.Library()

@register.filter(name='has_role')
def has_role(user, role_name):
    if user.is_anonymous:
        return False
    return user.userprofile.roles.filter(name=role_name).count() > 0


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter(name='add_css')
def addcss(field, css):
    return field.as_widget(attrs={"class": css})