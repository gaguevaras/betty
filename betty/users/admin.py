from django.contrib import admin

# Register your models here.
from django.core import urlresolvers
from users.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'company', 'user_email')
    list_filter = ('company', 'roles')

    def user_email(self, obj):
        return "%s" % (obj.user.email)

admin.site.register(UserProfile, UserProfileAdmin)