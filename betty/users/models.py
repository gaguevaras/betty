from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class UserRole(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):              # __unicode__ on Python 2
        return self.name


class CompanyManager(models.Manager):

    def admin_users(self, company_id):
        return self.users_by_role_name('admin', company_id)

    def manager_users(self, company_id):
        return self.users_by_role_name('manager', company_id)

    def teacher_users(self, company_id):
        return self.users_by_role_name('teacher', company_id)

    def users_by_role_name(self, user_role_name, company_id):
        return User.objects.filter(userprofile__company=company_id, userprofile__roles__name=user_role_name)


class Company(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    contact_name = models.CharField(max_length=200)
    contact_email = models.EmailField()

    objects = CompanyManager()

    def __str__(self):
        return self.name


class UserManager(models.Manager):

    def create_company(self, cleaned_data):
        # Create a company
        new_company = Company.objects.create(
            name=cleaned_data['company_name'],
            address=cleaned_data['address'],
            contact_name="%s %s" % (cleaned_data['first_name'], cleaned_data['last_name']),
            contact_email=cleaned_data['email']
        )
        from courses.models import CompanyActivity
        # Create a default set of activities for this company
        # These activities will be used as the default set for each course.
        activities = CompanyActivity.objects.filter(company_id=1)


        for activity in activities:
            CompanyActivity.objects.create(
                name=activity.name,
                type=activity.type,
                company=new_company
            )

        return new_company

    def create_profile(self, user, company, role_list):
        new_user_profile = UserProfile.objects.create(
            user=user,
            company=company
        )
        # Add a role to the new profile
        for role in role_list:
            new_user_profile.roles.add(role)

        return new_user_profile


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    roles = models.ManyToManyField(UserRole)
    company = models.ForeignKey(Company, null=True)
